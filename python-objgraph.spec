%global pypi_name objgraph

Name:           python-%{pypi_name}
Version:        3.4.0
Release:        1%{?dist}
Summary:        Draws Python object reference graphs with graphviz
License:        MIT
URL:            https://mg.pov.lt/objgraph/
Source0:        https://files.pythonhosted.org/packages/source/o/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(graphviz)
BuildRequires:  python3dist(mock)
BuildRequires:  python3dist(setuptools)

%global _description\
Python Object Graphs

%description %_description

%package -n python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}
%description -n python3-%{pypi_name} %_description

%prep
%autosetup -n %{pypi_name}-%{version}

%build
%{py3_build}

%install
%{py3_install}

%check
%{__python3} setup.py test

%files -n python3-%{pypi_name}
%license LICENSE
%doc README.rst
%{python3_sitelib}/__pycache__/*
%{python3_sitelib}/%{pypi_name}.py
%{python3_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%changelog
* Mon Feb 25 2019 Ken Dreyer <kdreyer@redhat.com> - 3.4.0-1
- Initial package.
